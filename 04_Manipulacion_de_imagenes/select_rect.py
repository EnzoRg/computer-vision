# Manolucos, Enzo - Legajo N° 74.868

import cv2
import sys
import numpy as np


def select_rect(event, x, y, flags, param):
    global ix, iy, fx, fy, img, img_backup, cropping
    if event == cv2.EVENT_LBUTTONDOWN:
        cropping = True
        ix, iy = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        cropping = False
        fx, fy = x, y
        cv2.rectangle(img, (ix, iy), (fx, fy), (255, 255, 255), 1)
    elif event == cv2.EVENT_MOUSEMOVE:
        if cropping is True:
            img = cv2.imread(filename)
            cv2.rectangle(img, (ix, iy), (x, y), (255, 255, 255), 1)


cropping = False
ix, iy = -1, -1		# Inicio
fx, fy = -1, -1		# Fin

if(len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pasar imagen como primer argumento')
    sys.exit(0)

img = cv2.imread(filename, cv2.IMREAD_COLOR)

cv2.namedWindow('Imagen')
cv2.moveWindow('Imagen', 150, 150)
cv2.setMouseCallback('Imagen', select_rect)

while(1):
    cv2.imshow('Imagen', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'):
        if ix > fx:
            ix = ix + fx
            fx = ix - fx
            ix = ix - fx
        if iy > fy:
            iy = iy + fy
            fy = iy - fy
            iy = iy - fy

        img_crop = np.zeros(((fy-iy+1), (fx-ix+1), 3), np.uint8)
        for i in range(1, (fy-iy)):
            for j in range(1, (fx-ix)):
                img_crop[i, j] = img[i+iy, j+ix]
        print('Imagen recortada: {} x {}'.format(j+2, i+2))		# +2px
        cv2.imwrite('img_crop.png', img_crop)
    elif k == ord('r'):
        img = cv2.imread(filename, cv2.IMREAD_COLOR)
        print('Imagen restaurada')
    elif k == ord('q'):
        print('Programa finalizado')
        break

cv2.destroyAllWindows()
