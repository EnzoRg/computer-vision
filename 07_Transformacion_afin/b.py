# Manolucos, Enzo Nicolas - Legajo N° 74.868

import cv2
import sys
import numpy as np


def transformar_afin(img, puntos_orig, puntos_nuevo):
	(h, w) = img.shape[:2]

	M = cv2.getAffineTransform(puntos_orig, puntos_nuevo)

	img_out = cv2.warpAffine(img, M, (w, h)) 

	return img_out


img = cv2.imread('palta.jpg')
(h, w) = img.shape[:2]

puntos_orig = np.float32([[0,0], [w-1, 0], [0, h-1]])
puntos_nuevo = np.float32([[w-1, h-1], [0, h-1], [w-1, 0]])

img = transformar_afin(img, puntos_orig, puntos_nuevo)

while (1):
	cv2.imshow('Imagen', img)
	if cv2.waitKey(1) & 0xFF == 27:
		break

cv2.destroyAllWindows()

