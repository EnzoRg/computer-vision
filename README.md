# Computer Vision 

This repository contains the projects carried out during "Visión por computadoras" course (UTN, FRC  🇦🇷 )


## Práctico 1 - Función en Python 

1. Crear una función **adivina** que permita adivinar un número secreto generado en
forma aleatoria, según las siguientes consignas:
   - El número secreto debe estar entre 0 y 100, y debe ser generado dentro de la función.
   - La función **adivina** debe recibir un parámetro que indique la cantidad de intentos permitidos.

2. Luego escribir un programa **adivinador.py** que:
   - pida al usuario que adivine el número secreto, es decir que ingrese un número entre 0 y 100,
   - use la función **adivina** anterior para verificar si adivinó (todo en el mismo archivo),
   - si el usuario adivinó el número secreto antes de superar la cantidad permitida de intentos, imprima un mensaje con el número de intentos en los que adivinó.
   - En caso de que esta cantidad de intentos sea superada el programa debe terminar con un mensaje.

3. Finalmente ejecutar el programa **adivinador.py** desde la consola.


## Práctico 2 - Segmentando una imagen

1. Escribir un programa en python que lea una imagen y realice un umbralizado binario, guardando el resultado en otra imagen.
   - NOTA: No usar ninguna función de las OpenCV, excepto para leer y guardar la imagen.

[Imagen original](https://gitlab.com/EnzoRg/computer-vision/-/blob/main/02_Segmentando_una_imagen/hojas.png)

[Resultado](https://gitlab.com/EnzoRg/computer-vision/-/blob/main/02_Segmentando_una_imagen/resultado.png)


## Práctico 3 - Propiedades de video

Considerando el siguiente [programa](https://gitlab.com/EnzoRg/computer-vision/-/blob/main/03_Propiedades_de_video/programa.py)

1. ¿Cómo obtener el frame rate o fps usando las OpenCV? Usarlo para no tener que *harcodear* el **delay** del **waitKey**.

2. ¿Cómo obtener el ancho y alto de las imágenes capturadas usando las OpenCV? Usarlo para no tener que *harcodear* el **frameSize** del video generado.


## Práctico 4 - Manipulación de imágenes

Considere el siguiente [código](https://gitlab.com/EnzoRg/computer-vision/-/blob/main/04_Manipulacion_de_imagenes/programa04.py)

1. Usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangular de una imagen, luego
   - con la letra “g” guardar la porción de la imagen seleccionada como una nueva imagen,
   - con la letra “r” restaurar la imagen original y permitir realizar una nueva selección,
   - con la “q” finalizar.


## Práctico 5 - Rotación + Traslación (o Transformación Euclidiana)

1. Crear una función que aplique una transformación euclidiana, recibiendo los siguientes parámetros:
   - Parámetros
       - **angle:**  Ángulo
	   - **tx:** traslación en x
	   - **ty:** traslación en y
   Recordar que la transformación euclidiana tiene la siguiente forma 
2. Usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangula de una imagen y
   - con la letra "e" aplique una transformación euclidiana a la porción de imagen seleccionada y la guarde como una nueva imagen.


## Práctico 6 - Transformación de similaridad

1. Agregar a la función anterior un parámetro que permita aplicar un escalado a la porción rectangular de imagen. 

   - Parámetros
       - **angle:** Ángulo
	   - **tx:** traslación en x
	   - **ty:** traslación en y
	   - **s:** escala
Recordar que la transformación de similaridad tiene la siguiente forma: 

2. Luego, usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangular de una imagen y
   - con la letra "s" aplique una transformación de similaridad a la porción de imagen seleccionada y la guarde como una nueva imagen. 
