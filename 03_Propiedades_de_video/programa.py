import sys
import cv2

if(len(sys.argv) > 1):
	filename = sys.argv[1]
else:
	print('Pass a filename as first argument')
	sys.exit(0)

cap = cv2.VideoCapture(filename)

fource = cv2.VideoWriter_fource('X', 'V', 'I', 'D')
framesize = (640, 480)
out = cv2.VideoWriter('output.avi', fource, 20.0, framesize)

delay = 33
while(cap.isOpened()):
	ret, frame = cap.read()
	if ret is True:
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		out.write(gray)
		cv2.imshow('Image gray', gray)
		if cv2.waitKey(delay) & 0xFF == ord('q'):
			break
	else:
		break

cap.realease()
out.release()
cv2.destroAllWindows()

