# Manolucos, Enzo - Legajo N° 74.868

import cv2

img = cv2.imread('hojas.png', 0)

# for row in range(len(img)):
# 	for col in range(len(img)):
# 		if img[row][col] < 235:
# 			img[row][col] = 0
# 		else:
# 			img[row][col] = 255

img[img < 235] = 0

print('Umbralizado binario realizado.')

cv2.imwrite('resultado.png', img)
