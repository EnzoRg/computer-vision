# Manolucos, Enzo - Legajo N° 74.868

import random


def adivina(intentos):

    numero = random.randint(0, 100)
    cont = 1

    while cont <= intentos:

        while True:
            try:
                a = int(input('Ingrese un número del 0 al 100: '))
                if a < 0 or a > 100:
                    raise ValueError
                break
            except ValueError:
                print('El número debe estar entre 0 y 100')

        if a == numero:
            print('Bien!')
            return cont
        else:
            print('Seguí intentandolo!')
            cont = cont+1


intentos = 3

print('Adivinaste en el intento: ', adivina(intentos))
