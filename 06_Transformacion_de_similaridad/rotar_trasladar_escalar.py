# Manolucos, Enzo - Legajo N° 74.868

import cv2
import sys
import numpy as np


def select_rect(event, x, y, flags, param):
    global ix, iy, fx, fy, img, img_backup, cropping

    if event == cv2.EVENT_LBUTTONDOWN:
        cropping = True
        ix, iy = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        cropping = False
        fx, fy = x, y
        cv2.rectangle(img, (ix, iy), (fx, fy), (255, 255, 255), 1)
    elif event == cv2.EVENT_MOUSEMOVE:
        if cropping is True:
            img = cv2.imread(filename)
            cv2.rectangle(img, (ix, iy), (x, y), (0, 0, 255), 1)


def rotacion_translacion(img, angle=0, tx=0, ty=0, s=1):
    M = np.float32([[s*np.cos(angle), s*np.sin(angle), tx],
                    [-s*np.sin(angle), s*np.cos(angle), ty]])
    (h, w) = img.shape[:2]
    img_out = cv2.warpAffine(img, M, (w, h))
    return img_out


cropping = False
ix, iy = -1, -1
fx, fy = -1, -1

if(len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pasar imagen como primer argumento')
    sys.exit(0)

img = cv2.imread(filename, cv2.IMREAD_COLOR)
cv2.namedWindow('Imagen')
cv2.moveWindow('Imagen', 150, 150)
cv2.setMouseCallback('Imagen', select_rect)

while (1):
    cv2.imshow('Imagen', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('e'):
        if ix > fx:
            ix = ix + fy
            fx = ix - fx
            ix = ix - fx
        if iy > fy:
            iy = iy + fy
            fy = iy - fy
            iy = iy - fy

        print('Atención! tener en cuenta la posición al rotar y escalar\n')

        angle = int(input('Ingrese ángulo: '))
        tx = int(input('Ingrese tx: '))
        ty = int(input('Ingrese ty: '))
        s = float(input('Ingrese el factor de escala: '))

        print('Transformación Euclidiana')
        img_out = np.zeros(((ty+fy-iy+1), (tx+fx-ix+1), 3), np.uint8)

        for i in range(1, (fy-iy)):
            for j in range(1, (fx-ix)):
                img_out[i, j] = img[i+iy, j+ix]
        img_out = rotacion_translacion(img_out, angle*np.pi/180, tx, ty, s)
        cv2.imwrite('resultado.jpg', img_out)
    elif k == ord('r'):
        img = cv2.imread(filename, cv2.IMREAD_COLOR)
        print('Imagen restaurada')
    elif k == ord('q'):
        print('Programa finalizado')
        break

cv2.destroyAllWindows()
